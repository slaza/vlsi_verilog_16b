module bin_to_ss(
	input wire [4:0] input_value,
	output reg [6:0] output_value
);

	always @ (*) begin
		case(input_value)
			0:			output_value = 7'b100_0000;
			1:			output_value = 7'b111_1001;
			2:			output_value = 7'b010_0100;
			3:			output_value = 7'b011_0000;
			4:			output_value = 7'b001_1001;
			5:			output_value = 7'b001_0010;
			6:			output_value = 7'b000_0010;
			7:			output_value = 7'b111_1000;
			8:			output_value = 7'b000_0000;
			9:			output_value = 7'b001_1000;
			
			10:		output_value = 7'b010_0000;	
			11:		output_value = 7'b000_0011;
			12:		output_value = 7'b100_0110;
			13:		output_value = 7'b010_0001;
			14:		output_value = 7'b000_0110;
			15:		output_value = 7'b000_1110;
			default:	output_value = 7'b111_1111;	//iskljuceno	
		
		endcase
	
	end
	
endmodule
