module test();
	reg clk;
	reg reset;
	reg button0;
	reg sw0;
	
	wire [7:0] led;
	wire [6:0] hex0;
	wire [6:0] hex1;
	wire [6:0] hex2;
	wire [6:0] hex3;
	
	defparam m0.MILI_SEKUNDA = 1;
	defparam m0.POLA_SEKUNDE = 5;
	defparam m0.SEKUNDA = 10;
	defparam m0.DVE_SEKUNDE = 20;
	defparam m0.CETIRI_SEKUNDE = 40;
	
	main m0(
		clk, 
		reset, 
		button0, 
		sw0, 
		led, 
		hex0, 
		hex1, 
		hex2, 
		hex3
	);
	
	initial begin
		$dumpfile("dump.vcd");
		$dumpvars(0, test);
	
		clk = 0;
		reset = 0;
		button0 = 0;
		sw0 = 0;
	end
	
	always  begin
		#1 clk = 0;
		#1 clk = 1;
	end

	initial begin
		#2 reset = 1;
		#2 reset = 0;
		
		//inicijalizacija
			//1
			#2 button0 = 1;
			#4 button0 = 0;
			//2
			#2 button0 = 1;
			#4 button0 = 0;
			//3
			#2 button0 = 1;
			#4 button0 = 0;
			//4
			#2 button0 = 1;
			#4 button0 = 0;
			//5
			#2 button0 = 1;
			#4 button0 = 0;
			//6
			#2 button0 = 1;
			#4 button0 = 0;
			//7
			#2 button0 = 1;
			#4 button0 = 0;
			//8
			#2 button0 = 1;
			#4 button0 = 0;
			//9
			#2 button0 = 1;
			#4 button0 = 0;
			//10
			#2 button0 = 1;
			#4 button0 = 0;
			//11
			#2 sw0 = 1;
			#2 button0 = 1;
			#4 button0 = 0;
			//12
			#2 button0 = 1;
			#4 button0 = 0;
			//13
			#2 button0 = 1;
			#4 button0 = 0;

			//14
			#2 button0 = 1;
			#4 button0 = 0;
			//15
			#2 button0 = 1;
			#4 button0 = 0;
			//16
			#2 button0 = 1;
			#4 button0 = 0;
			#2 sw0 = 0;
		
			//prelazak na selekciju
			#1000 button0 = 1;
			#40 button0 = 0;	//2.5s
		//selekcija
			//segment 1
			#10 sw0 = 1;
			#2 button0 = 1;
			#4 button0 = 0;
			#2 sw0 = 0;
			
			#4 sw0 = 1;
			#2 button0 = 1;
			#4 button0 = 0;
			#2 sw0 = 0;
			
			#4 button0 = 1;
			#4 button0 = 0;
			
			#2 sw0 = 1;
			#2 button0 = 1;
			#4 button0 = 0;
			#2 sw0 = 0;
			
			//segment 2
			#10 sw0 = 1;
			#2 button0 = 1;
			#4 button0 = 0;
			#2 sw0 = 0;
			
			#4 sw0 = 1;
			#2 button0 = 1;
			#4 button0 = 0;
			#2 sw0 = 0;
			
			#4 button0 = 1;
			#4 button0 = 0;
			
			#2 button0 = 1;
			#4 button0 = 0;
			
			//segment 3
			#10 button0 = 1;
			#4 button0 = 0;
			
			#2 button0 = 1;
			#4 button0 = 0;
			
			#4 button0 = 1;
			#4 button0 = 0;
			
			#2 button0 = 1;
			#4 button0 = 0;

			//segment 4
			#10 button0 = 1;
			#4 button0 = 0;
			
			#2 button0 = 1;
			#4 button0 = 0;
			
			#4 button0 = 1;
			#4 button0 = 0;
			
			#2 button0 = 1;
			#4 button0 = 0;
			
		//prikaz
			//prelazak na selekciju
			#50 button0 = 1;
			#40 button0 = 0;	//2.5s
			
			//segment 1
			#10 sw0 = 1;
			#2 button0 = 1;
			#4 button0 = 0;
			#2 sw0 = 0;
			
			#4 sw0 = 1;
			#2 button0 = 1;
			#4 button0 = 0;
			#2 sw0 = 0;
			
			#2 sw0 = 1;
			#2 button0 = 1;
			#4 button0 = 0;
			#2 sw0 = 0;
			
			#4 button0 = 1;
			#4 button0 = 0;
			
			//segment 2
			#2 button0 = 1;
			#4 button0 = 0;
			
			#2 button0 = 1;
			#4 button0 = 0;
			
			//izlazak drzanjem 4s
			#10 button0 = 1;
			#80 button0 = 0;
			
			#600 $finish;
	end
	/*
	initial #15000 $finish;
	*/
endmodule
