/*
	main.v
*/ 

module main (
	input wire clk,
	input wire reset,
	input wire button0,
	input wire sw0,
	
	output reg [7:0] led,
	output wire [6:0] hex0,
	output wire [6:0] hex1,
	output wire [6:0] hex2,
	output wire [6:0] hex3
	);
	
	//stanja
	localparam INICIJALIZACIJA = 3'b000;
	localparam PRIKAZ = 3'b001;
	localparam SELEKCIJA = 3'b010;
	reg [2:0] state_reg;
	reg [2:0] state_next;
	
	//registar
	reg [15:0] r0_input_par;
	reg r0_load_par;
	reg r0_shift_l;
	reg r0_input_shift;
	wire [15:0] r0_output_value;
	
	//brojac 0-15
	reg [4:0] preostalo_za_unos_next;
	reg [4:0] preostalo_za_unos_reg;
	
	//displej
	
	localparam OFF_D = 5'h10;
	
	reg [4:0] disp_0;
	reg [4:0] disp_1;
	reg [4:0] disp_2;
	reg [4:0] disp_3;

	//red
	wire red_b0;
	
	//prikaz
	reg [1:0] cifra_za_prikaz_reg;
	reg [1:0] cifra_za_prikaz_next;
	
	//brojac
	parameter MILI_SEKUNDA = 50_000;
	parameter POLA_SEKUNDE = 25_000_000;
	
	parameter SEKUNDA = 50_000_000;
	parameter DVE_SEKUNDE = 100_000_000;
	parameter CETIRI_SEKUNDE = 200_000_000;
	
	integer counter_reg, counter_next;
	integer mili_sec_cnt_reg, mili_sec_cnt_next;
	
	//3
	integer drzi_counter_reg, drzi_counter_next;
	reg [2:0] selektovana_cifra_reg;
	reg [2:0] selektovana_cifra_next;
	
	//4
	reg [15:0] nova_vrednost_reg;
	reg [15:0] nova_vrednost_next;
	
	reg [3:0] segment_nove_vrednosti_reg;
	reg [3:0] segment_nove_vrednosti_next;
	
	reg [2:0] uneto_u_segment_reg;
	reg [2:0] uneto_u_segment_next;

	//modules start
	reg16 r0(
		.clk(clk),
		.reset(reset),
		.load_par(r0_load_par),
		.input_paralel(r0_input_par),
		.shift_l(r0_shift_l),
		.input_shift(r0_input_shift),
		.output_value(r0_output_value)
	);

	red red0(
		.clk(clk),
		.reset(reset),
		.input_value(button0),
		.output_value(red_b0)
	);
	
	bin_to_ss bin0(.input_value(disp_0), .output_value(hex0));	//input_value, output_value
	bin_to_ss bin1(.input_value(disp_1), .output_value(hex1));
	bin_to_ss bin2(.input_value(disp_2), .output_value(hex2));
	bin_to_ss bin3(.input_value(disp_3), .output_value(hex3));
	
	//modules end
	
	always @ (posedge clk, posedge reset) begin
		if(reset) begin 
			//1
			state_reg <= INICIJALIZACIJA;
			preostalo_za_unos_reg <= 5'h10;	//16 binarno
			//2
			cifra_za_prikaz_reg <= 0;
			mili_sec_cnt_reg <= 0;
			counter_reg <= 0;
			//3
			drzi_counter_reg <= 0;
			selektovana_cifra_reg <= 0;
			//4
			nova_vrednost_reg <= 0;
			segment_nove_vrednosti_reg <= 0;
			uneto_u_segment_reg <= 0;
		end else begin
			//1
			state_reg <= state_next;
			preostalo_za_unos_reg <= preostalo_za_unos_next;
			//2
			cifra_za_prikaz_reg <= cifra_za_prikaz_next;
			mili_sec_cnt_reg <= mili_sec_cnt_next;
			counter_reg <= counter_next;
			//3
			drzi_counter_reg <= drzi_counter_next;
			selektovana_cifra_reg <= selektovana_cifra_next;
			//4
			nova_vrednost_reg <= nova_vrednost_next;
			segment_nove_vrednosti_reg <= segment_nove_vrednosti_next;
			uneto_u_segment_reg <= uneto_u_segment_next;
		end
	end
	
	always @ (*) begin
		state_next = state_reg;
		r0_input_par = 0;
		r0_load_par = 0;
		r0_shift_l = 0;
		r0_input_shift = 0;
		
		//off
		disp_0 = OFF_D;
		disp_1 = OFF_D;
		disp_2 = OFF_D;
		disp_3 = OFF_D;
		
		//off
		led = 0;
		
		preostalo_za_unos_next = preostalo_za_unos_reg;
		
		//2
		cifra_za_prikaz_next = cifra_za_prikaz_reg;
		mili_sec_cnt_next = mili_sec_cnt_reg;
		counter_next = counter_reg;
		
		//3
		drzi_counter_next = drzi_counter_reg;
		selektovana_cifra_next = selektovana_cifra_reg;
		
		//4
		segment_nove_vrednosti_next = segment_nove_vrednosti_reg;
		nova_vrednost_next = nova_vrednost_reg;
		uneto_u_segment_next = uneto_u_segment_reg;
		
		case(state_reg)
			INICIJALIZACIJA:
				begin
					//output
					led[7:4] = preostalo_za_unos_reg[3:0];
					led[3:0] = r0_output_value[3:0];
					
					//input
					if(red_b0 == 1) begin
						r0_input_shift = sw0;
						r0_shift_l = 1;
						
						preostalo_za_unos_next = (preostalo_za_unos_reg == 0)? 0: preostalo_za_unos_reg - 1;
					end
					//next state
					if(preostalo_za_unos_reg == 0) begin
						state_next = PRIKAZ;
						cifra_za_prikaz_next = 0;
						mili_sec_cnt_next = 0;
						counter_next = 0;
					end
				end
			PRIKAZ:
				begin
					
					//na clock
					counter_next = counter_reg + 1;
					
					if(counter_reg == MILI_SEKUNDA) begin
						counter_next = 0;
						mili_sec_cnt_next = mili_sec_cnt_reg + 1;
					end
					
					
					//na trazeni broja milisekundi
					if(mili_sec_cnt_reg == r0_output_value) begin
						cifra_za_prikaz_next = cifra_za_prikaz_reg + 1;	//vrti u krug
						mili_sec_cnt_next = 0;
					end
					
					//output
					case(cifra_za_prikaz_reg)
						2'b00:	disp_0 = r0_output_value[3:0];
						2'b01:	disp_1 = r0_output_value[7:4];
						2'b10:	disp_2 = r0_output_value[11:8];
						2'b11:	disp_3 = r0_output_value[15:12];
					endcase
					
					//3
					if(button0 == 1) begin
						drzi_counter_next = (drzi_counter_reg == DVE_SEKUNDE)? DVE_SEKUNDE : drzi_counter_reg + 1;
					end else begin
						drzi_counter_next = 0;
					end
					
					//next state
					if(button0 == 0 && drzi_counter_reg == DVE_SEKUNDE) begin	//promeni stanje kada se dugme otpusti
						state_next = SELEKCIJA;
						selektovana_cifra_next = 0;
						counter_next = 0;
						drzi_counter_next = 0;
						//4
						segment_nove_vrednosti_next = 0;
						nova_vrednost_next = 0;
						uneto_u_segment_next = 0;
					end
				end
			SELEKCIJA:
				begin
					
					
					
					//output
					disp_0 = r0_output_value[3:0];
					disp_1 = r0_output_value[7:4];
					disp_2 = r0_output_value[11:8];
					disp_3 = r0_output_value[15:12];
					
					led[3:0] = segment_nove_vrednosti_reg;
					
					counter_next = (counter_reg == SEKUNDA)? 0 : counter_reg + 1;
					
					/*
						na osnovu maske i countera prikazi cifru na displeju
						sve cifre su prikazane osim selektovane koja blinka
					*/
					if(counter_reg <= POLA_SEKUNDE) begin
						case(selektovana_cifra_reg)
							0:
							begin
								disp_0 = OFF_D;
							end
							1:
							begin
								disp_1 = OFF_D;
							end
							2:
							begin
								disp_2 = OFF_D;
							end
							3:
							begin
								disp_3 = OFF_D;
							end
						endcase
					end
					
					//input
					//drzimo dugme0
					if(button0 == 1) begin
						drzi_counter_next = (drzi_counter_reg == CETIRI_SEKUNDE)? CETIRI_SEKUNDE : drzi_counter_reg + 1;
					end else begin
						drzi_counter_next = 0;
					end
					

					
					/*
						to se cuva nova uneta vrednost
							reg [15:0] nova_vrednost_reg;
							reg [15:0] nova_vrednost_next;
						
						
						cuva se 4-ka koja se menja trenutno
							reg [3:0] segment_nove_vrednosti_reg;
							reg [3:0] segment_nove_vrednosti_next;
						
						cuva se br bita koji su uneti u jedan segment
							reg [2:0] uneto_u_segment_reg;
							reg [2:0] uneto_u_segment_next; 
						
						trenutni segment koji se menja
							reg [2:0] selektovana_cifra_reg;
							reg [2:0] selektovana_cifra_next;
					*/
					
					
					//next state
					if(selektovana_cifra_reg == 4) 
						begin
							/*
								obisli sve segmente, vracamo se
							*/
							r0_load_par = 1;
							r0_input_par = nova_vrednost_reg;
							
							counter_next = 0;
							mili_sec_cnt_next = 0;
							
							selektovana_cifra_next = 0;
							nova_vrednost_next = 0;
							segment_nove_vrednosti_next = 0;
							
							state_next = PRIKAZ;
						end
					else if(button0 == 0 && drzi_counter_reg == CETIRI_SEKUNDE) 
						begin	
							/*
								Na drzanje 4s i pustanje dugmeta unutar nekog segmenta
								U sledecem taktu cemo izaci
							*/
							selektovana_cifra_next = 4;
						end
					else if((button0 == 0 && drzi_counter_reg >= DVE_SEKUNDE) || (uneto_u_segment_reg == 4))
						begin	
							/*
								Na drzanje [2-4)s i pustanje dugmeta ili unos 4b u segment
								
								Sacuvamo vrednost segmenta i prelazimo na sledeci
								Ako je u pitanju treci segment, u sledecem taktu izlazimo
							*/
							if(selektovana_cifra_reg == 3)
								begin
									//zapamti cetvrti blok
									nova_vrednost_next = {segment_nove_vrednosti_reg, nova_vrednost_reg[11:0]};	
								end 
							else if(selektovana_cifra_reg == 2) 
								begin
									//zapamti treci blok
									nova_vrednost_next = {nova_vrednost_reg[15:12], segment_nove_vrednosti_reg, nova_vrednost_reg[7:0]};
								end 
							else if(selektovana_cifra_reg == 1) 
								begin
									//zapamti drugi blok
									nova_vrednost_next = {nova_vrednost_reg[15:8], segment_nove_vrednosti_reg, nova_vrednost_reg[3:0]};	
								end 
							else if(selektovana_cifra_reg == 0) 
								begin
									//zapamti prvi blok
									nova_vrednost_next = {nova_vrednost_reg[15:4], segment_nove_vrednosti_reg};	
								end
							
							/*
								Promeni na sledeci segment
							*/
							segment_nove_vrednosti_next = 0;
							uneto_u_segment_next = 0;
							selektovana_cifra_next = selektovana_cifra_reg + 1;
						end 	//button0 == 0 && drzi_counter_reg >= DVE_SEKUNDE
						
						else if(button0 == 0 && drzi_counter_reg < DVE_SEKUNDE && drzi_counter_reg > 0) 
							begin	
								/*
									Na drzanje (0,2) i pustanje dugmeta
								*/
								
								segment_nove_vrednosti_next = {segment_nove_vrednosti_reg[3:0], sw0};	//vrednost segmenta
								uneto_u_segment_next = uneto_u_segment_reg + 1;
								
									/*
										Kada je uneto_u_segment_reg == 4, preci ce se u sledeci segment, kao i da smo drzali 4s
									*/
							end		//button0 == 0 && drzi_counter_reg < DVE_SEKUNDE && drzi_counter_reg > 0
				end	//case
		endcase
	end
	
endmodule
