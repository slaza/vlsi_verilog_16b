module reg16(
	input wire clk,
	input wire reset,

	input wire load_par,
	input wire [15:0] input_paralel,
	
	input wire shift_l,
	input wire input_shift,
	
	output reg[15:0] output_value
);

	reg [15:0] value_reg;
	reg [15:0] value_next;
	
	always @ (posedge clk, posedge reset) begin
		if(reset == 1) begin 
			value_reg <= 0;
		end else begin
			value_reg <= value_next;
		end
	end
	
	always @ (*) begin
		value_next = value_reg;
		
		if(load_par == 1) begin
			value_next = input_paralel;
		end else if(shift_l == 1) begin
			value_next[15:0] = {value_reg[14:0], input_shift};
		end
		
		output_value = value_reg;
	end
	
	
endmodule
